#!/usr/bin/env python3

import json
import subprocess as sp
from argparse import ArgumentParser as AP


#def get_args():
#    parser = AP(description="Custom module to use pomd in waybar.")
#    parser.add_argument("commands",
#        help="Possible command to execute.",
#        choices = ["toggle"]
#    )
#
#    return parser.parse_args()


def main():
#    args = get_args()

    pomc_path = "/home/griff/.cargo/bin/pomc"
    status = json.loads(sp.run([pomc_path, "status", "-j"],
        capture_output=True).stdout.decode('ascii').strip().strip('\x00'))

    if not status:
        return

    remaining_min = status["remaining"]["secs"] // 60
    remaining_sec = status["remaining"]["secs"] % 60
    remaining = f"{remaining_min}:{remaining_sec}"
    return_json = {
        "text": status["phase"],
        "tooltip": f"Remaining: {remaining}\nCompleted rounds: {status['completed_rounds']}",
        "alt": "None",
        "percentage": "None",
        "class": "custom/pomd",
    }
    print(json.dumps(return_json))


if __name__ == '__main__':
    main()
